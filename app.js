//first
const paragraph = document.querySelectorAll('p');
for(let i = 0; i < paragraph.length; i++) {
    paragraph[i].style.background = '#ff0000';
}

//second
const list = document.querySelector('#optionsList');
console.log(list);
console.log(list.parentElement);
const childNodes = list.childNodes;

let arr = [];
for(let i = 0; i < childNodes.length; i++) {
    arr.push([childNodes[i].nodeName, childNodes[i].nodeType]);
}
console.log(arr);

//third
const newTextParagraph = document.querySelector('#testParagraph');
newTextParagraph.textContent = 'This is a paragraph';
console.log(newTextParagraph)

//fourth
const elemMainHeader = document.querySelector('.main-header');
const arrElemMainHeader = elemMainHeader.childNodes;
console.log(arrElemMainHeader);

for(let i = 0; i < arrElemMainHeader.length; i++) {
    arrElemMainHeader[i].className = 'nav-item';
}
console.log(arrElemMainHeader);

//fifth
const searchClass = document.querySelectorAll('.section-title');
console.log(searchClass)

for(let i = 0; i < searchClass.length; i++) {
    searchClass[i].classList.remove('section-title');
}
console.log(searchClass);
